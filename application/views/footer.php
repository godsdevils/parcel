    <!----------------------------------- Start Footer ----------------------------------->
    
    <footer id="footer">
      <div class="footerlink FltLt footerlinkM">
        <div class="footerlinkTop">
          <div class="footerThree">
            <h3>Useful Stuff</h3>
          </div>
          <div class="footerThree">
            <h3> <a id="Footer_lnkMyAccount" href="javascript:popFromHeader('','myaccount.aspx')">myAccount</a></h3>
          </div>
          <div class="footerThree">
            <h3>Information Hub</h3>
          </div>
        </div>
        <div class="footerlinkMid">
          <div class="footerThree footerThreeM"> <a href="<?php echo $base_url ?>staticpages/tracking">Track a Packet</a><br>
            <a href="<?php echo $base_url ?>staticpages/bulkshipping">Bulk Shipping</a><br>
            <a href="<?php echo $base_url ?>staticpages/faq">Claims Information</a><br>
            <a href="<?php echo $base_url ?>staticpages/help">Contact Us / Help</a><br>
          </div>
          <div class="footerThree footerThreeM"> <a href="#">myOrders</a><br>
          </div>
          <div style="width: 149px;" class="footerThree footerThreeM"> <a href="<?php echo $base_url ?>staticpages/couriercompanies">Courier Companies</a><br>
            <a href="<?php echo $base_url ?>staticpages/courierservice">Courier Services</a><br>
            <a href="<?php echo $base_url ?>staticpages/faq">FAQ</a><br>
            <a href="<?php echo $base_url ?>staticpages/aboutus">About Us</a><br>
          </div>
        </div>
        <div class="footerlinkBtm"> </div>
      </div>
      <div class="footerlink FltRt">
        <div class="footerlinkTop">
          <div style="text-align:center" class="footerTwo">
            <h3><a href="/hub/send-a-parcel-to/europe/ ">Payment Options</a></h3>
          </div>
        </div>
        <div class="footerlinkMid" align="center"> <br />
          <br />
          <img src="<?php echo $base_url; ?>images/logos/payment.gif" alt=""> </div>
        <div class="footerlinkBtm"> </div>
      </div>
      <div class="clr"></div>
    </footer>

    <div class="copyrt"> Copyright 2012 - 2013. All Rights Reserved </div>

    <!----------------------------------- End Footer ----------------------------------->
  
</div>

<!----------------------------------- End Main Section ----------------------------------->

</body>
</html>
