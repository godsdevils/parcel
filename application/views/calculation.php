    
    <!----------------------------------- Page Body Section ----------------------------------->

  <div id="PageCont">


	<div id="breadcrumb">

        <ul>
        	<li><a href="#">Home</a></li>
            <li> - <strong>Search Result Page</strong></li>
            <br class="clr" />
        </ul>
    
    </div>

	<div id="leftnav">
    <br />
    <p class="text14 textBld">Your delivery quotes from the courier are listed below. Please select a service to ship your packets</p>
        <ul class="resdetails">
            <li class="coll">Collecting From:</li>
            <li class="del">Delivery To:</li>
            <li class="weight">1 Packet - Weight <?php echo $parcelWeight ?>kg</li>
            <br class="clr" />
        </ul>

    	<div class="resultsec">
        	<ul class="hd">
            	<li class="com">Courier Company</li>
            	<!--li class="del">Delivery Time</li-->
            	<li class="char">Charges<span style="font-size:8px">(approx)</span></li>
            	<li class="price">Our Price</li>
                <br class="clr" />
            </ul>
            <?php foreach($courierList as $key=>$val){ ?>
        	<ul class="grey Lgrey">
            	<li class="com">
              <?php if(!empty($val['image'])) { ?>
              <img src="<?php echo $val['image']; ?>" alt="<?php echo $val['name'];  ?>" class="logo" />
              <?php } else { echo $val['name']; } ?>
              </li>
            	<!--li class="del">2 Days</li-->
            	<li class="char"><img src="images/icons/black-rupee.gif" />&nbsp;&nbsp; <?php echo 6*$val['net_charges']; ?> </li>
            	<li class="price"><img src="images/rupee.gif" />&nbsp;&nbsp; <?php echo $val['net_charges']; ?> </li>
            	<li class="buy"><a href="#"><img src="images/buttons/booknow.gif" /></a></li>
                <br class="clr" />
            </ul>
            <?php } ?>
        </div>
    </div>

	<div id="rightnav">
    <br /><br /><br /><br /><br />
    	<a href="#"><img src="images/banner3.jpg" /></a>
    </div>
    <br class="clr" />

    <div class="pad20"></div>
	
<!--	<section id="FeaturedSer">
      <h2 class="heading">Featured Services</h2>
      <div class="item FltLt"> <img src="images/f-lt.gif" class="FltLt" />
        <h3 class="subhd FltLt">Mumbai <br />
          to <br />
          Delhi</h3>
        <img src="images/f-rt.gif" class="FltRt" /> <span class="price FltRt">495</span> <img src="images/booknow.png" class="booknow FltRt" /> <br class="clr" />
      </div>
      <div class="item FltLt"> <img src="images/f-lt.gif" class="FltLt" />
        <h3 class="subhd FltLt">Mumbai <br />
          to <br />
          Delhi</h3>
        <img src="images/f-rt.gif" class="FltRt" /> <span class="price FltRt">495</span> <img src="images/booknow.png" class="booknow FltRt" /> <br class="clr" />
      </div>
      <div class="item FltLt"> <img src="images/f-lt.gif" class="FltLt" />
        <h3 class="subhd FltLt">Mumbai <br />
          to <br />
          Delhi</h3>
        <img src="images/f-rt.gif" class="FltRt" /> <span class="price FltRt">495</span> <img src="images/booknow.png" class="booknow FltRt" /> <br class="clr" />
      </div>
      <div class="item last FltLt"> <img src="images/f-lt.gif" class="FltLt" />
        <h3 class="subhd FltLt">Mumbai <br />
          to <br />
          Delhi</h3>
        <img src="images/f-rt.gif" class="FltRt" /> <span class="price FltRt">495</span> <img src="images/booknow.png" class="booknow FltRt" /> <br class="clr" />
      </div>
      <br class="clr" />
    </section>
	
    <br class="clr" />-->
    
    <div class="pad20"></div>
    <div class="pad20"></div>
    <section id="logosSec">
      <h3>Choose From The Worlds Top Couriers</h3>
      <a href="#"><img src="images/Aramex.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/Bluedart.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/Fedex logo.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/FF logo.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/ups.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/logos/tnt-sml-logo.png" class="marNon" alt="" height="80" width="130"></a> 
      <br class="clr" />
    </section>
    
    
  </div>
  
    <!----------------------------------- End Body Section ----------------------------------->


