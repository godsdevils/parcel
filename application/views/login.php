
    <!----------------------------------- Page Body Section ----------------------------------->

  <div id="PageCont">


	<div id="breadcrumb">

        <ul>
        	<li><a href="#">Home</a></li>
            <li> - <strong>Sign In / Register</strong></li>
            <br class="clr" />
        </ul>
    
    </div>

	<div id="signin">
    	<h2 class="heading">Please Sign In</h2>
      <form action="/login" method="post">
        <div class="form">
        	<span class="sec">
                <label class="Lsec">Email Address:</label>
                <input name="email" id="txtName" type="text" value="" class="nml" value="" placeholder="Please Enter Your Email" />
                <br class="clr" />
            </span>
        	<span class="sec">
                <label class="Lsec">Password:</label>
                <input name="password" id="txtName" type="password" value="" class="nml" value="" placeholder="Please Enter Your Password" />
                <br class="clr" />
            </span>
        	<span class="section">
                <label class="Lsec">Remember me:</label>
                <input name="txtName" id="txtName" type="checkbox" value="Postcode" class="remMe FltLt" value="" />
                <input type="image" alt="Submit" value="btnSubmitReg" name="btnSubmitReg" id="btnSubmitReg" class="signin FltLt" src="images/buttons/signin.png" />
                <span class="forgot FltLt"><a href="#">Forgotten your Password?</a></span>
                <br class="clr" />
            </span>
        </div>
        </form>
    </div>

	<div id="registerSec">
    	<h2 class="heading">New to bookmypacket.com?</h2>
        <div class="box">
            <p class="text14">
            <br /><br /><br />
            <strong>If you don’t have an account with us, registering is easy! Just click the button below and fill in your details. It’ll only take a few seconds!</strong>
            <br /><br /><br /><br />
            </p>
            <input type="image" alt="Submit" value="btnSubmitReg" name="btnSubmitReg" src="images/buttons/register.png" />
		</div>
    </div>
    <br class="clr" />
    
    <div class="pad100"></div>
    <div class="pad100"></div>
    <section id="logosSec">
      <h3>Choose From The Worlds Top Couriers</h3>
      <a href="#"><img src="images/Aramex.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/Bluedart.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/Fedex logo.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/FF logo.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/ups.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/logos/tnt-sml-logo.png" class="marNon" alt="" height="80" width="130"></a> 
      <br class="clr" />
    </section>
    
    
  </div>
  
    <!----------------------------------- End Body Section ----------------------------------->
