<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
<title>India's first online courier service for domestics and International</title>
<link rel="Stylesheet" href="css/main.css">
<link rel="Stylesheet" href="css/innerPages.css">
<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/html5.js"></script>
</head>
<body>

<!----------------------------------- Start Main Section ----------------------------------->

<div id="mainCont">

    <!----------------------------------- Start Header Section ----------------------------------->
  <header id="HdrCont">
    <h1 class="logo"> <a href="#">Packet</a> </h1>
    <ul class="toplink">
      <li><a href="#">Register</a></li>
      <li><a href="#">Sign In</a></li>
    </ul>
    <div id="Search">
      <h2>Packet Tracking</h2>
      <input type="text" name="" class="search" id="" class="trackItInput" />
      <input type="image" alt="Submit" value="btnSubmitReg" name="" id="" src="images/go.gif" />
    </div>
    <br class="clr" />
  </header>
  
    <!----------------------------------- End Header Section ----------------------------------->

    <!----------------------------------- Start Navigation Section ----------------------------------->

  <nav>
    <ul class="nav">
      <li><a href="#">Home</a></li>
      <li><a href="#">Courier Services</a></li>
      <li><a href="#">Courier Companies</a></li>
      <li><a href="#">Tracking</a></li>
      <li><a href="#">BUlk Shipping</a></li>
      <li class="last"><a href="#">Help &amp; Contact</a></li>
      <br class="clr" />
    </ul>
  </nav>

    <!----------------------------------- End Navigation Section ----------------------------------->

    <!----------------------------------- Page Body Section ----------------------------------->

  <div id="PageCont">


	<div id="breadcrumb">

        <ul>
        	<li><a href="#">Home</a></li>
            <li> - <strong>Collection Detail</strong></li>
            <br class="clr" />
        </ul>
    
    </div>

	<div id="leftnav">

    	<div class="myAccColldet">
            <h2>Collaction Details</h2>
            <div class="pad10">
            	<div class="note">
                	<strong>I would like my packet collecting on:</strong> <select class="searchFromDateField" name="_$ddlStatementMonth"><option value="1">Tuesday 28 May 2013</option></select>
                	<p><strong>Please Note:</strong> Collections can occure any time between <strong>8:30am &amp; 6pm</strong> on the day of collection. unfortunately, we <strong>CANNOT</strong> provide you with a specific time, nor advise a time when a courier will arrive on the day of collection - so please ensure someone is at the premises from <strong>8:30am to 6pm</strong> to hand the packet(s)to the driver.</p>
                </div>
                <p class="notetxt">Please enter collection address details below. The information in these boxes must be accurate otherwise your courier may not be able to collect your packet(s)</p>
                <div class="pad10"></div>
                <div class="form FltLt">
                    <span class="address">
                        Address Book
                    </span>
                    <span class="sec">
                        <label class="Lsec">Country: <strong class="red">*</strong></label><label class="txtsec">UK Mainland</label>
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Postalcode: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="post" value="" />
                        <input type="submit" class="btn" id="" value="Lookup" name="">
                    </span>
                    <span class="sec">
                        <label class="Lsec">Select Address: <strong class="red">*</strong></label>
                        <select name="" disabled="disabled" class="nml"><option value="1">Tuesday 28 May 2013</option></select>
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">&nbsp;</label>
                        <span class="pencil">Enter Address Manualy</span>
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Company Name: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Address 1: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Address 2: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Address 3:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Town: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">County:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Postalcode: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Country: <strong class="red">*</strong></label><label class="txtsec">UK Mainland <strong class="red">*</strong></label>
                        <br class="clr" />
                    </span>
                    <span class="info">
                        Add to Address Book? &nbsp; <input name="txtName" id="txtName" type="checkbox" value="" value="" />
                    </span>
                </div>
                <div class="form FltRt">
                    <span class="sec">
                        <label class="Lsec">Contact Name: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Phone Number: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Mobile Number:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Email Address: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <div class="collNote pad10">
                    	<strong class="text14">Collection Note (max 20 characters)</strong>
                        <p class="notetxt">Please be aware that notes are passed to the collection driver. However we are unable to verify that the driver will read them</p>
                        <textarea class="areaCommon" rows="" cols="" id="txtFAddr" name="txtFAddr"></textarea>
                    </div>
                </div>
                <div class="clr pad10"></div>
            </div>
        </div>
        
        <div class="pad10"></div>
        
        <div class="myAccColldet">
            <h2>Delivery Details</h2>
            <div class="pad10">
                <p class="notetxt">Please enter collection address details below. The information in these boxes must be accurate otherwise your courier may not be able to collect your packet(s)</p>
                <div class="pad10"></div>
                <div class="form FltLt">
                    <span class="address">
                        Address Book
                    </span>
                    <span class="sec">
                        <label class="Lsec">Country: <strong class="red">*</strong></label><label class="txtsec">UK Mainland</label>
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Postalcode: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="post" value="" />
                        <input type="submit" class="btn" id="__btnViewAccountFundsHistory" value="Lookup" name="_$btnViewAccountFundsHistory">
                    </span>
                    <span class="sec">
                        <label class="Lsec">Select Address: <strong class="red">*</strong></label>
                        <select name="" disabled="disabled" class="nml"><option value="1">Tuesday 28 May 2013</option></select>
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">&nbsp;</label>
                        <span class="pencil">Enter Address Manualy</span>
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Company Name:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Address 1: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Address 2: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Address 3:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Town: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">County:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>

                    <span class="sec">
                        <label class="Lsec">Postalcode: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Country: <strong class="red">*</strong></label><label class="txtsec">UK Mainland <strong class="red">*</strong></label>
                        <br class="clr" />
                    </span>
                    <span class="info">
                        Add to Address Book? &nbsp; <input name="txtName" id="txtName" type="checkbox" value="" value="" />
                    </span>
                </div>
                <div class="form FltRt">
                    <span class="sec">
                        <label class="Lsec">Contact Name: <strong class="red">*</strong></label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Phone Number:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Mobile Number:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec">Email Address:</label>
                        <input name="" id="" type="text" value="" class="nml" value="" />
                        <br class="clr" />
                    </span>
                    <span class="sec">
                        <label class="Lsec info">Notify:</label>
                        <input name="" id="" type="checkbox" value="" value="" />
                        <br class="clr" />
                    </span>
                    <div class="collNote pad10">
                    	<strong class="text14">Delivery Note (max 20 characters)</strong>
                        <p class="notetxt">Please be aware that notes are passed to the collection driver. However we are unable to verify that the driver will read them</p>
                        <textarea class="areaCommon" rows="" cols="" id="txtFAddr" name="txtFAddr"></textarea>
                    </div>
                </div>
                <div class="clr pad10"></div>
            </div>
        </div>
        
    </div>

	<aside id="rightnav">
    	<div class="myAccPaySum pad10">
        	<h3>Booking Summary</h3>
            <p>
            	<strong>Packets</strong>: 1<br />
                <strong>Courier</strong>: City Link<br />
                <strong>Service</strong>: City Link Next Day<br />
                <br />
                <strong>From</strong>: GB<br />
                <strong>To</strong>: GB
            </p>
        	<h3>Price Summary</h3>
            <p>
                <strong class="Larea">Price:</strong><span class="Sarea">&euro; 8.49</span>
                <strong class="Larea">Additional:</strong><span class="Sarea">0.00</span>
                <strong class="Larea">Cover:</strong><span class="Sarea">0.00</span>
                <br class="clr" /><br />
            </p>
			<p class="subtotal">
                <strong class="Larea">Subtotal:</strong><span class="Sarea">&euro; 8.49</span>
                <br class="clr" />
            </p>
			<p>
                <strong class="Larea">VAT:</strong><span class="Sarea">&euro; 1.70</span>
                <br class="clr" />
            </p>
            <div class="total">
            	<strong>Total: &euro; 1.70</strong>
            </div>

        </div>
        
        <div class="pad10"></div>
        
        <div class="myAccPromocode pad10">
        	<h3>Got a Promotional code?</h3>
            <input name="" id="" type="text" value="" class="nml" value="" />
            <input type="submit" class="btn" id="" value="Submit" name="">
            
        </div>
        
        <div class="pad10"></div>
               	<input type="image" alt="Submit" value="btnSubmitReg" name="btnSubmitReg" src="images/buttons/proceed.png" />
        
    </aside>
    <br class="clr" />

    <div class="pad20"></div>
	
	
    <br class="clr" />
    
    <div class="pad20"></div>
    <section id="logosSec">
      <h3>Choose From The Worlds Top Couriers</h3>
      <a href="#"><img src="images/logos/citylink-sml-logo.png" alt=""></a> <a href="#"><img src="images/logos/collect-sml-logo.png" alt=""></a> <a href="#"><img src="images/logos/hermes-sml-logo.png" alt=""></a> <a href="#"><img src="images/logos/my-economy-logo.png" alt=""></a> <a href="#"><img src="images/logos/my-express-logo.png" alt=""></a> <a href="#"><img src="images/logos/sprint-sml-logo.png" alt=""></a> <a href="#"><img src="images/logos/tnt-sml-logo.png" class="marNon" alt=""></a> <br class="clr" />
    </section>
    
    
  </div>
  
    <!----------------------------------- End Body Section ----------------------------------->

    <!----------------------------------- Start Footer ----------------------------------->
    
    <footer id="footer">
      <div class="footerlink FltLt footerlinkM">
        <div class="footerlinkTop">
          <div class="footerThree">
            <h3>Useful Stuff</h3>
          </div>
          <div class="footerThree">
            <h3> <a id="Footer_lnkMyAccount" href="javascript:popFromHeader('','myaccount.aspx')">myAccount</a></h3>
          </div>
          <div class="footerThree">
            <h3>Information Hub</h3>
          </div>
        </div>
        <div class="footerlinkMid">
          <div class="footerThree footerThreeM"> <a href="#">Track a Packet</a><br>
            <a href="#">Bulk Shipping</a><br>
            <a href="#">eBay Shipping</a><br>
            <a href="#">Claims Information</a><br>
            <a href="#">Contact Us / Help</a><br>
          </div>
          <div class="footerThree footerThreeM"> <a href="#">myOrders</a><br>
            <a href="#">myDeliveries</a><br>
            <a href="#">Statements</a><br>
            <a href="#">Address Book</a><br>
            <a href="#">Account Settings</a><br>
          </div>
          <div style="width: 149px;" class="footerThree footerThreeM"> <a href="#">Courier Companies</a><br>
            <a href="#">Courier Services</a><br>
            <a href="#">International Services</a><br>
            <a href="#">FAQ</a><br>
            <a href="#">About Us</a><br>
          </div>
        </div>
        <div class="footerlinkBtm"> </div>
      </div>
      <div class="footerlink FltRt">
        <div class="footerlinkTop">
          <div style="text-align:center" class="footerTwo">
            <h3><a href="/hub/send-a-parcel-to/europe/ ">Payment Options</a></h3>
          </div>
        </div>
        <div class="footerlinkMid" align="center"> <br />
          <br />
          <img src="images/logos/payment.gif" alt=""> </div>
        <div class="footerlinkBtm"> </div>
      </div>
      <div class="clr"></div>
    </footer>

    <div class="copyrt"> Copyright 2012 - 2013. All Rights Reserved </div>

    <!----------------------------------- End Footer ----------------------------------->

    
    <div class="copyrt"> Copyright 2012 - 2013. All Rights Reserved </div>

  
</div>

<!----------------------------------- End Main Section ----------------------------------->

</body>
</html>
