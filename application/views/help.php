<body>
<div class="static" style="margin-left:410px">
Help and Contact.
Check Helping Hand!</div><div class="static">
Our aim is to make the entire customer experience of booking a packet through bookmypacket.com as simple as possible. However, we understand that from time to time our customers require additional support and assistance.</div><div class="static">
1. FAQ's and Online Help Documents</div><div class="static">
Most customer enquiries can be quickly and easily answered by referring to our extensive FAQ page. </div><div class="static">

Please submit your query in below box. Our customer care team will revert you within 24 hours. </div><div class="static">

Terms & Conditions</div><div class="static">

1. In these conditions : </div><div class="static">
a. "Company" means BMP e-group solution pvt ltd.</div><div class="static">
b. "Goods" means any documents and packets which are accepted by the Company for carriage (including containers and packaging) from one address to another.</div><div class="static">
c. "Prohibited Items" means and includes all goods and materials, the carriage of which is prohibited by any laws, rules and regulations applicable to the carriage, including dangerous and hazardous, combustible or explosive materials, as well as drugs, live or dead plants or animals, foods, cosmetic, liquor, fine arts, antiques, precious metals, and stones, gold and silver in any form, currency (paper or coin) credit cards and traveller's cheques, negotiable Securities and certificates and any other negotiable items, material that could be considered as pornographic or offensive or politically sensitive.</div><div class="static">
d. "Customer means and includes the Sender, consignor, consignee, holder of this consignment note, receiver and owner of the goods, or any other party having a legal interest in the goods. </div><div class="static">
e. "Sender means the person or organisation ordering the carriage.</div><div class="static">
2. The Company is not a common carrier and accepts goods for convenience on these conditions only. No servant or agent of the company has authority to alter, vary or waive any provision in the contract in any respect.</div><div class="static">
3. The Company accepts goods for conveyance on the basis that (a) to (e) of these conditions are all fulfilled and the customer warrants that they are fulfilled: </div><div class="static">
a. that the customer is either the owner of or acting as fully authorised agent of the owner of the goods and that if any other person has an interest on the goods the customer is acting as his fully authorised agent also;</div><div class="static">
b. that the goods do not comprise a letter or letter which the post office has an exclusive right to convey;</div><div class="static">
c. that the goods do not contain prohibited items as described above;</div><div class="static">
d. that the goods are not fragile;</div><div class="static">
4. The customer shall pay to the company for each consignment of goods in accordance with the company's current tariff of charges, copies of which are available on request from the company's places of business. Payments shall be made promptly and in full. No deductions shall be made on account of any alleged claims against the company for compensation or otherwise. However sender is and shall always remain responsible and liable as principle debtor / obligor for all freight charges, duties, taxes and for all other payments, expenses, fines, penalties, loss or damage incurred or suffered by the company or its agents in connection with the goods. All other customers are jointly and severally liable in this respect.</div><div class="static">
5. The company is entitled to convey goods : </div><div class="static">
a. by its own servants and / or by any airline, delivery company / and or other independent contractors;</div><div class="static">
b. by any means of conveyance;</div><div class="static">
c. by any route whatsoever;</div><div class="static">
6. If for any reason beyond the company's control it is unable to convey the goods to the address to which they are consigned or to effect delivery at the said address; </div><div class="static">
a. the company shall endeavour to communicate with the customers and request a new address to which the goods can be delivered on the country in which they are then lying.</div><div class="static">
b. If the company is unable to communicate with the customer within a reasonable time or if it is not provided with a new address for delivery the company shall be a liberty to deal with the goods in accordance to condition 15 hereinafter set out or to destroy them.</div><div class="static">
7. The company has the right to but not the obligation, to inspect any shipment including, without limitation, opening the shipment.</div><div class="static">
8. The company shall be liable to pay compensation for loss, damage, misdelivery or delay occuring in respect of the goods caused solely by its own negligence or the negligence of its own servants, subject to the following; </div><div class="static">
a. compensation shall not be payable in respect of indirect of consequential loss;</div><div class="static">
b. the said loss, damage, misdelivery or delay shall have been reported to the company within 3 days from the date of delivery;</div><div class="static">
c. any claim or compensation shall have been intimated within three days of the said loss, damage, misdelivery or delay having been reported to the company </div><div class="static">
d. any legal proceedings relating to a claim for compensation shall be commenced within the three months of the consignment of the goods.</div><div class="static">
9. Except as set out in condition 8, the company shall not be in any liability whatsoever in respect of goods.</div><div class="static">
10. Without prejudice to the generality of condition 9 and for the avoidance of doubt the company shall not be liable whatsoever in respect of any losses caused; </div><div class="static">
a. partly by its negligence and / or the negligence of its servants and partly by the negligence of the customer.</div><div class="static">
b. by its independent contractors in any manner whatsoever </div><div class="static">
11. The company's servants on whose behalf the company contracts with the customer shall not be in any liability whatsoever in respect of goods. </div><div class="static">
1. While the company will endeavour to exercise its best efforts to provide expeditious delivery in accordance with regular delivery schedules, the company, will not, under any circumstances be liable for any delay in pick-up, transportation or delivery of any shipment regardless of the cause of such delay.</div><div class="static">
2. Further, the company shall not be liable for any loss, damage, misdelivery or non delivery; </div><div class="static">
a. due to act of god, force majeure occurrence, war, riot, hijack or any cause reasonably beyond the control of the company;</div><div class="static">
b. caused by the act default or ommission of the Sender, the consignee of any other party who claims an interest in the shipment (including violation of any terms and conditions hereof or of any person other than the company, or of any customs or postal services or any other Government officials, forwarder or other entity or person to whom a shipment is tendered by the company for transportation to any location not regularly served by the company regardless whether the sender requested or had knowledge of such third party arrangements;</div><div class="static">
c. caused by the nature of the shipment of any defect, characteristic or inherent vice thereof;</div><div class="static">
d. caused by electrical or magnetic injury, erasure or other such damage to electronic or photographic images or recordings in any form.</div><div class="static">
2. In the event of any loss, damage, delay or misdelivery occurring in respect of goods by reason of any acts or default of an independent contractor employed by the company and / or while the goods are in the possession of such an independent contractor the company shall at the request of the customer assign to the customer and right of action the company may have against the independent contractor.</div><div class="static">
3. The customer shall indemnify the company against; </div><div class="static">
a. any expenses incurred as a result of its inability for any reason beyond its control to convey or delivery goods to the address to which they were consigned or at all;</div><div class="static">
b. any claims, costs and demands by third parties, relating to the goods;</div><div class="static">
4. The Company shall have a lien for any amount due under the contract and the costs of recovering the same, if any lien is not satisfied within a reasonable time the company may sell the goods either privately or by auction and apply the proceeds in or towards the discharge of the lien and the expenses of sale.</div><div class="static">
5. This contract is governed by Indian Law and any dispute arising under it shall within the exclusive jurisdiction of the Indian Courts.</div><div class="static">


2. Pickup solutions  </div><div class="static">
Although our couriers do achieve a very high level of successful collections on time, it is occasionally possible for a collection to be delayed. The main reasons for this are:</div><div class="static">
Adverse weather conditions which can sometimes affect a courier’s route</div><div class="static">
Traffic congestion, road closures and traffic accidents may also cause a courier to take a longer route</div><div class="static">
Rebook a Collection</div><div class="static">
If a courier has not come to collect your packet, you may book a new collection by clicking the button below. Please be aware that we can only process a re-booking with a courier after 6pm on the day of collection. This is because couriers will continue to collect up until 5.30pm on any given day.</div><div class="static">
3. Delivery  Updates</div><div class="static">
Sometimes it is possible that circumstances out of our hands (such as extreme adverse weather conditions or road closures) may affect the collection & delivery times of your order. We will update all of our customers of such potential issues - as and when they could occur - from this page - which you should check before making any important deliveries.</div><div class="static">
Tack your packet </div><div class="static">
Just put your BMP ref no. in Tracking page . </div><div class="static">
4. Claims</div><div class="static">
Before making any claim, you need to check few things. </div><div class="static">
Check your item is covered under our compensation scheme. As stated during the booking process, prohibited and restricted items are not covered under our compensation cover policy. Please double check your item is eligible for compensation by reading our Prohibited & Restricted Items list under Terms and Conditions. </div><div class="static">
Check that the timescale for submitting your claims has not been exceeded. We must receive your claim and all the documents within 5 working days of the scheduled delivery date. If the deadline for a claim has passed then unfortunately no claim will be accepted.</div><div class="static">
Check that you have all the documents necessary for you claim to be submitted. </div><div class="static">
The documents needed are for any type of claim are: </div><div class="static">
A suitable proof of value - Website screenshots will not be accepted</div><div class="static">
Additional documents are then needed depending on what type of claim you are making: </div><div class="static">
For a damage claim you will need: </div><div class="static">
Proof of repair</div><div class="static">
Photos of the damage</div><div class="static">
Photos of the internal and external packaging that was used </div><div class="static">
You will also need to ensure the item is available for collection should it need to be investigated. </div><div class="static">
For a claim for loss you will need: </div><div class="static">
A proof of loss. This can be a letter or e-mail from the intended receiver to advise they have not received the item. </div><div class="static">
If any documents are missing or not submitted within the deadlines the claim will not be accepted.</div><div class="static">
Once you have checked the above you will need to complete our claim form on the next page.</div><div class="static">
We do aim to have all claims completed within 4 weeks from receiving the claim and we will keep you updated on the progress of your claim. A lost claim can only be processed once the carrier has made extensive searches and deems the goods as lost. This process can take up to 40 working days to be completed.</div><div class="static">
Please note that we can only process a claim up to the amount of compensation cover purchased on your order. If no additional cover was purchased then the item will only be covered for the standard amount included on the service booked.</div><div class="static">

Contact Us Directly</div><div class="static"></div><div class="static">
If you can't find what you're looking for on our website, feel free to contact us directly.</div><div class="static">
Tried Yourself </div><div class="static">
Most collection and delivery questions can be answered by our online help pages outlined below. Please be sure to check these before contacting us directly, as it will greatly speed up finding a resolution to your issue.</div><div class="static">
Link of tracking, FAQ’s , Pickup issue, Delivery issue. </div><div class="static">

A link Submit a Support Ticket</div>
</body>
