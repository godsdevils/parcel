

    <!----------------------------------- Page Body Section ----------------------------------->

  <div id="PageCont">
    <section id="detailSec">
      <ul class="Hd">
        <li class="Hd1 FltLt">
          <h2 class="heading"><span class="FltLt">1</span>Enter Delivery Details</h2>
        </li>
        <li class="Hd2 FltLt">
          <h2 class="heading"><span class="FltLt">2</span>Enter your Parcel Details</h2>
        </li>
        <li class="Hd3 FltLt">
          <h2 class="heading"><span class="FltLt">3</span>Choose your Courier Service</h2>
        </li>
        <br class="clr" />
      </ul>
    </section>
   <form action="/calculation" method="post" id="info"> 
    <section id="detailForm">
      <div class="deliveryDet FltLt">
        <div class="pad05"></div>
        <span class="sec">
        <label class="Lsec">From:</label>
        <input name="sourcecode" id="ddlCollectionCountry" type="text" value="" class="nml" value="" placeholder="Please Enter source City"/>
        <!--select name="sourcecode" class="nml" id="ddlCollectionCountry" class="bPDropDown" tabindex="10">
        <option value="176" selected="selected">Delhi</option>
        </select-->
        <br class="clr" />
        </span> <span class="sec">
        <label class="Lsec">&nbsp;</label>
        <input name="source-pincode" id="txtName" type="text" value="" class="nml" value="" placeholder="Please Enter source Pincode"/>
        <br class="clr" />
        </span> <span class="sec">
        <label class="Lsec">To:</label>
        <input name="destinationcode" id="ddlCollectionCountry" type="text" value="" class="nml" value="" placeholder="Please Enter destination City"/>
        <!--select name="destinationcode" class="nml" id="ddlCollectionCountry" tabindex="10">
          <option value="176" selected="selected">Maharastra</option>
        </select-->
        <br class="clr" />
        </span> <span class="sec">
        <label class="Lsec">&nbsp;</label>
        <input name="destination-pincode" id="" value="" type="text" class="nml" placeholder="Please Enter destination Pincode"/>
        <br class="clr" />
        </span> <!--span class="sec">
        <label class="Lsec">Date</label>
        <input name="date" id="" type="text" value="03/04/2013" class="nml" maxlength="15" />
        <br class="clr" />
        </span--> </div>
      <div class="parcelDet FltLt">
        <div class="pad05"></div>
        <span class="sec">
        <label class="Lsec">No. of Parcel:</label>
        <select name="nparcel" class="nml" id="ddlCollectionCountry" class="bPDropDown" tabindex="10">
        <option value="1" "selected">1</option>
        <option value="2" >2</option>
        <option value="3" >3</option>
        <option value="4" >4</option>
        <option value="5" >5</option>
        <option value="6" >6</option>
        <option value="7" >7</option>
        <option value="8" >8</option>
        <option value="9" >9</option>
        <option value="10" >10</option>
        </select>
        <label class="Lsec">Units:</label>
        <select name="unitlength" class="nml" id="ddlCollectionCountry" class="bPDropDown" tabindex="10">
        <option value="176" selected="selected">cm</option>
        </select>
        <select name="unitweight" class="nml" id="ddlCollectionCountry" class="bPDropDown" tabindex="10">
        <option value="176" selected="selected">kg</option>
        </select>
        <br class="clr" />
        </span>
        <div class="pad20"></div>
        <span class="txt"> &nbsp; </span> <span class="section">
        <label class="Lsec">Length</label>
        <input name="length" id="" type="text" value="" class="nml" value="" placeholder="0"/>
        <br class="clr" />
        </span> <span class="txt"> x </span> <span class="section">
        <label class="Lsec">Width</label>
        <input name="width" id="" type="text" value="" class="nml" value="" placeholder="0"/>
        <br class="clr" />
        </span> <span class="txt"> x </span> <span class="section">
        <label class="Lsec">Height</label>
        <input name="height" id="" type="text" value="" class="nml" value="" placeholder="0"/>
        <br class="clr" />
        </span> <span class="txt txt2"> cm </span> <span class="section">
        <label class="Lsec">Weight</label>
        <input name="weight" id="" type="text" value="" class="nml" value="" placeholder="0"/>
        <br class="clr" />
        </span> <span class="txt txt2"> kg </span> </div>
      <div class="getquotes FltLt">
        <input type="image" alt="Submit" value="btnSubmitReg" name="btnSubmitReg" id="btnSubmitReg" src="images/getquotes.jpg" onclick="document.getElementById('info').submit();"/>
      </div>
      <br class="clr" />
    </section>
   </form> 
<!--    
    <div class="pad20"></div>
    <section id="FeaturedSer">
      <h2 class="heading">Featured Services</h2>
      <div class="item FltLt"> <img src="images/f-lt.gif" class="FltLt" />
        <h3 class="subhd FltLt">Mumbai <br />
          to <br />
          Delhi</h3>
        <img src="images/f-rt.gif" class="FltRt" /> <span class="price FltRt">495</span> <img src="images/booknow.png" class="booknow FltRt" /> <br class="clr" />
      </div>
      <div class="item FltLt"> <img src="images/f-lt.gif" class="FltLt" />
        <h3 class="subhd FltLt">Mumbai <br />
          to <br />
          Delhi</h3>
        <img src="images/f-rt.gif" class="FltRt" /> <span class="price FltRt">495</span> <img src="images/booknow.png" class="booknow FltRt" /> <br class="clr" />
      </div>
      <div class="item FltLt"> <img src="images/f-lt.gif" class="FltLt" />
        <h3 class="subhd FltLt">Mumbai <br />
          to <br />
          Delhi</h3>
        <img src="images/f-rt.gif" class="FltRt" /> <span class="price FltRt">495</span> <img src="images/booknow.png" class="booknow FltRt" /> <br class="clr" />
      </div>
      <div class="item last FltLt"> <img src="images/f-lt.gif" class="FltLt" />
        <h3 class="subhd FltLt">Mumbai <br />
          to <br />
          Delhi</h3>
        <img src="images/f-rt.gif" class="FltRt" /> <span class="price FltRt">495</span> <img src="images/booknow.png" class="booknow FltRt" /> <br class="clr" />
      </div>
      <br class="clr" />
    </section>
-->    
    
    <div class="pad20"></div>
    <div id="Ads"> <img src="images/banner1.gif" class="booknow FltLt" /> <img style="width:449px; height:112px;" src="images/banner2.gif" class="booknow FltRt" /> <br class="clr" />
    </div>
    <div class="pad20"></div>
    <script type="text/javascript">
    $(document).ready(function () {
      try{
        $("#featured > ul").tabs({ fx: { opacity: "toggle", duration: "slow"} }).tabs("rotate", 5000, true);
      }
      catch(e){}
    });

</script>
    <section id="featuredAll">
      <div class="featuredTitle">
        <h2> Not sure how it works? <span class="greenHow">Here's a guide...</span></h2>
      </div>
      <div id="featured">
        <ul class="ui-tabs-nav">
          <li class="ui-tabs-nav-item" id="nav-fragment-1"><a href="#fragment-1"> <span>1. Enter Parcel Details</span><img src="images/icons/arr.png" /></a></li>
          <li class="ui-tabs-nav-item ui-tabs-selected" id="nav-fragment-2"><a href="#fragment-2"><span>2. Select a Courier</span><img src="images/icons/arr.png" /></a></li>
          <li class="ui-tabs-nav-item" id="nav-fragment-3"><a href="#fragment-3"><span>3. Get your Labels</span><img src="images/icons/arr.png" /></a></li>
          <li class="ui-tabs-nav-item" id="nav-fragment-4"><a href="#fragment-4"><span>4. Parcel Collection</span><img src="images/icons/arr.png" /></a></li>
          <li class="ui-tabs-nav-item" id="nav-fragment-5"><a href="#fragment-5"><span>5. Track your Parcel</span><img src="images/icons/arr.png" /></a></li>
          <li class="ui-tabs-nav-item" id="nav-fragment-6"><a href="#fragment-6"><span>6. Delivered!</span><img src="images/icons/arr.png" /></a></li>
        </ul>
        <!-- First Content -->
        <div id="fragment-1" class="ui-tabs-panel ui-tabs-hide" style=""> <img src="images/parcel-details.jpg" class="slide" alt="">
          <div class="info">
            <h2> 1. Enter your packet details</h2>
            <p> Got a packet to send? Simply enter your required 
              delivery and packet information above and then click ‘Get Quotes’!</p>
          </div>
        </div>
        <!-- Second Content -->
        <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" style="opacity: 0.655822; display: block;"> <img src="images/courier-selection.jpg" class="slide" alt="">
          <div class="info">
            <h2> 2. Select your courier</h2>
            <p> We’ll supply you with a list of courier services 
              available for your delivery; all you need to do is choose one.</p>
          </div>
        </div>
        <!-- Third Content -->
        <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" style=""> <img src="images/get-labels.jpg" class="slide" alt="">
          <div class="info">
            <h2> 3. Getting your labels</h2>
            <p> After selecting a courier, we’ll ask you just a few 
              more details in order to process your booking. We’ll then provide you 
              with a shipping label for each packet (if required).</p>
          </div>
        </div>
        <!-- Fourth Content -->
        <div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" style=""> <img src="images/packet-collection.jpg" class="slide" alt="">
          <div class="info">
            <h2> 4. Parcel collection</h2>
            <p> Your chosen courier will collect your packet(s) 
              directly from your doorstep, so all you have to do is sit back and wait!</p>
          </div>
        </div>
        <div id="fragment-5" class="ui-tabs-panel ui-tabs-hide" style=""> <img src="images/trace-your-parcel.jpg" class="slide" alt="">
          <div class="info">
            <h2> 5. Tracking your packet</h2>
            <p> You can track your packet every step of the way 
              until it reaches its destination.<!--, by using our handy, online, <a href="http://www.myparceldelivery.com/instanttracking.aspx">instant tracking tool</a>. </p>-->
          </div>
        </div>
        <div id="fragment-6" class="ui-tabs-panel ui-tabs-hide" style="display:none;"> <img src="images/parcel-delivered.jpg" class="slide" alt="">
          <div class="info">
            <h2> 6. Delivered</h2>
            <p> Within no time at all, your packet will be safely delivered. Pretty simple really!</p>
          </div>
        </div>
      </div>
    </section>
    
    <div class="pad20"></div>
    <div class="pad20"></div>
    <section id="logosSec">
      <h3>Choose From The Worlds Top Couriers</h3>
      <a href="#"><img src="images/Aramex.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/Bluedart.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/Fedex logo.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/FF logo.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/ups.JPG" alt="" height="80" width="130"></a> 
      <a href="#"><img src="images/logos/tnt-sml-logo.png" class="marNon" alt="" height="80" width="130"></a> 
      <br class="clr" />
    </section>
    
    
  </div>
  
    <!----------------------------------- End Body Section ----------------------------------->


