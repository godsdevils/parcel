<div class="static" style="margin-left:410px">
General Enquires
</div><div class="static">
<b>Q. How do bookmypacket.com’s service’s work?</b></div><div class="static">
A. Before gaining a quote from our website, you must first ensure you have the accurate weight and dimensions for each packet you are sending.</div><div class="static">
<b>Q. What services does bookmypacket.com offer?</b></div><div class="static">
A. bookmypacket.com offers a wide range of delivery options to its customers, including same-day, next-day, 2 day and 3-5 day services. In addition, we are able to facilitate both National and international deliveries – allowing you to send a packet to over 241 countries worldwide!</div><div class="static">
<b>Q. How do I know which is the best service for me?</b></div><div class="static">
A. Entirely dependent on your needs, your choice of service should reflect how quickly you require your packet to be delivered. By specifying your packet’s dimensions, weight and destination during the booking process, we will provide you with a list of available delivery options that you can select from.</div><div class="static">
<b>Q. What is the minimum number of packets I can send with bookmypacket.com?</b></div><div class="static">
A. There is no minimum or maximum! You can use bookmypacket.com to send 1 packet or 100's – the sky’s the limit!</div><div class="static">
Instant quotes and Booking</div><div class="static">
<b>Q. How do I gain a quote from bookmypacket.com ?</b></div><div class="static">
A. Before gaining a quote from our website, you must first ensure you have the accurate weight and dimensions for each packet you are sending.</div><div class="static">
1. Log on to www.bookmypacket.com.
2. Enter the weight and dimensions of your packet(s) into the booking form at the top of the page, along with your required collection and destination country from the drop-down box. For multiple packets, simply select a quantity from the drop-down list (uncheck the tick box next to the drop-down if your packets differ in weight and/or size).
3. Click the ‘Get Free Quote’ button for a list of service options & prices. You can navigate through the varying delivery options by clicking on the service tabs at the top of the results.</div><div class="static">
<b>Q. How are the quotes I receive calculated?</b></div><div class="static">
A. The quotes you receive are calculated based on:
The size of your packets
The weight of your packets
Where the packet is travelling to and from
The service you require – e.g. next day, 2-3 day, etc.
The calculations will take into account the density of your packets, to allow for an accurate quote based on the cubic space and weight they occupy on the couriers vehicle. There is no need to calculate this yourself, as bookmypacket.com™ will automatically compute the correct value for you.</div><div class="static">
<b>Q. How do I book an order with bookmypacket.com?</b></div><div class="static">
A. To book an order with bookmypacket.com , you must first obtain an instant quote from our website (you can do this by following the steps outlined above).</div><div class="static">
Upon gaining a quote, simply click the green ‘Buy Now’ button next to the delivery service you wish to book. If you are a returning customer this will forward you onto the booking page, where you should enter just a few details as to your packets’ contents, along with its collection and delivery address. If you are a new customer, you will be required to complete a quick registration form before proceeding to the booking page.Only after successfully completing all required fields on the booking page, will you be able to continue on and pay for your order. We accept all major credit/debit cards and also take payment via PayPal.</div><div class="static">
<b>Q. Will I receive a VAT receipt?</b></div><div class="static">
A. Yes – you can access this in three ways:
1. After successful payment, we will provide you with the option to download the VAT invoice.
2. We will supply you with a link to your VAT invoice in your order confirmation email.
3. You can access the VAT invoice from the myDeliveries page in the myAccount section of website.</div><div class="static">
<b>Q. Do I need a printer to use bookmypacket.com’s services?</b></div><div class="static">
A. Depending on the service you book, you may be required to print shipping labels and other documents for your packet. Any service that requires you to do so will display a printer required icon next it – so be sure to check before placing your order!</div><div class="static">
<b>Q. Can I place an order over the phone with bookmypacket.com™?</b></div><div class="static">
A. Currently, we cannot facilitate this option. Since each order is required to be linked to an individual account, you must be logged in to bookmypacket.com™ to book a service</div><div class="static">

Issue At ordering
<b>Q. Can I still place an order if I am unsure of the exact size and weight of my packet(s)?</b></div><div class="static">
A. In order to provide you with an accurate quote, we will require the exact weight and dimensions of your packet. If you book an order with incorrect measurements, you will be liable to pay any surcharge that incurs as a result. In addition, the courier may refuse to collect your packet if it exceeds the measurements that you supply.</div><div class="static">
<b>Q. Why do I encounter an error when placing my order?</b></div><div class="static">
A. Most errors encountered whilst placing an order on bookmypacket.com™ are due to details being entered incorrectly. In the first instance we would advise you to check over the details you have provided during the booking process. If all details are correct and the error still occurs, then please contact our customer support team for further assistance.</div><div class="static">
<b>Q. Why do I see a ‘booking failed’ page after I make a payment?</b></div><div class="static">
A. Your booking may have failed for a number of reasons. It is important NOT to re-book your packet at this point. The failure will be picked up and dealt with by our customer support team and you will be informed via email when the issue has been resolved.</div><div class="static">
If the booking cannot be placed, you will be advised as to why this is and a full refund to you will be processed.</div><div class="static">
If you have any concerns, please contact our customer support team for assistance.</div><div class="static">
Booking Query</div><div class="static">
<b>Q. I have entered the incorrect details on my order, what do I need to do?</b></div><div class="static">
A. You should contact our customer support team at the earliest convenience. If details change after or immediately prior to collection, you will be required to pay a surcharge to compensate the courier costs incurred.</div><div class="static">
<b>Q. Can I add additional packets to my order once it has been booked?</b></div><div class="static">
A. Unfortunately not, if you have additional packets that need to be collected you will need to book these on a separate order.</div><div class="static">
<b>Q. I have several packets to ship to different delivery addresses, what can I do?</b></div><div class="static">
A. You will be required to book separate orders for each packet. Each packet will need to be labeled with the appropriate delivery address before they are collected.</div><div class="static">
If you have selected different courier services for each packet, please ensure that you aware which courier should be picking up which packet when they arrive. In the event that the incorrect packet is given to the courier, additional surcharges may be incurred.</div><div class="static">
<b>Q. I need to cancel my order, what should I do?</b></div><div class="static">
A. To cancel your order, please contact our customer support team at least 24 hours prior to the collection date for a full refund. We will not be able to offer a refund on any order that is cancelled beyond this notice period.</div><div class="static">
Label on Packet..</div><div class="static">
<b>Q. What labels do I need to attach to my packet?</b></div><div class="static">
A. Some of our couriers require you to attach a shipping label to your packet(s). If this is the case, you will see a printer required icon next the courier logo on our results page. It is very important that you secure these labels to your packet(s), as they signify your delivery was booked with bookmypacket.com™.</div><div class="static">
Any packets sent via our printer required services which do not have a label attached, will be subject to additional charges from the courier.
Should you receive a bill form the courier, please forward on all the following details on Marketing@bookmypacket.com
1. Courier Invoice
2. BMP  Order number
3. Paperwork left by the driver
4. Any other documents received by the courier
On receipt of this information, bookmypacket.com™ will attempt to resolve the issue on your behalf.
</div><div class="static">
Packaging of Packet</div><div class="static">
<b>Q. How should my item(s) be packaged?</b></div><div class="static">
A. All packets sent through bookmypacket.com™ should be packaged according to our guidelines, which can be found here. This will reduce the risk of damage to any item sent during transit. Any packet sent which does not comply with these guidelines will be sent at your own risk, and as such no claim can be raised against them.</div><div class="static">
Here are a few handy tips to bear in mind when packaging your packet:</div><div class="static">
Be sure to use internal padding such as bubble wrap, foam sheets & Polychips</div><div class="static">
Use an undamaged cardboard box to place your item(s) in</div><div class="static">
If you have any concerns when packing your packet, please don’t hesitate to contact our customer support team for advice.</div><div class="static">
<b>Q. My item is not packaged, can I still send it?</b></div><div class="static">
A. Because unpackaged items face risk of damage during transit, our couriers may refuse to collect any packet that is insufficiently protected/packed and is not fit for transit. Unfortunately, no refund will be issued should a driver refuse to collect a packet based on these grounds.</div><div class="static">
<b>Q. Can I still send my packet if it does not follow the packaging guidelines?</b></div><div class="static">
A. Any packet not packaged in-line with our guidelines may be sent at the customers own risk, but no claim will be accepted should the item(s) packed sustain any damage during transit.</div><div class="static">
Couriers reserve the right to refuse a collection should they believe the packet is not fit for transit.</div><div class="static">
Unfortunately, no refund will be issued should a driver refuse to collect a packet based on these grounds</div><div class="static">

Pickup Qurery.</div><div class="static">
<b>Q. Can I specify a time for a collection?</b></div><div class="static">
A. Yes – you can specify a preferred time for collection when booking an order. Our couriers will do all they can to accommodate your needs; however, it is not always possible to collect exactly on time.</div><div class="static">
Collections are generally between 9am and 5.30pm</div><div class="static">
<b>Q. I have booked a service, what time can I expect the driver to collect?</b></div><div class="static">
A. Most couriers operate between the hours of 9am and 5.30pm; however, in some cases this may be extended to 7pm.</div><div class="static">
<b>Q. I have booked a service but entered the wrong collection day, what should I do?</b></div><div class="static">
A. Contact our customer support team with the correct details and they will be able to assist. If the collection has already been attempted by this stage, a surcharge may be applied to your order.</div><div class="static">
<b>Q. I have a collection booked and need to leave the house, what should I do?</b></div><div class="static">
A. In some cases, our couriers will collect from a neighbor’s address. In this case, you should leave a note on your door to advise the courier of the location of your packet.</div><div class="static">
Please note – it is down to the driver’s discretion as to whether they will collect your packet from an alternative address. Should the collection be missed you will be required to pay a surcharge to re-book the service, which will need to be paid in full before the order is re-submitted to the courier.</div><div class="static">
<b>Q. No driver has arrived to collect my packets, what now?</b></div><div class="static">
A. You should contact our customer support team at the earliest convenience, who will help to resolve the issue for you and re-book the collection. It is important to do this, as following a failed collection – a courier will not always call the next today to collect your packet.</div><div class="static">
<b>Q. I missed my collection and the courier left a card, how do I re-arrange?</b></div><div class="static">
A. You should contact our customer support team, who will assist you in arranging to have your packet(s) collected on another day.</div><div class="static">

Tracking issue</div><div class="static">
<b>Q. Where can I find my tracking number?</b></div><div class="static">
A. You can find your tracking number in your confirmation email upon successful completion of your order (this will begin with MPD). Should you be the one receiving the packet, you will be required to contact the sender for your tracking number.</div><div class="static">
<b>Q. Where can I track my packet?</b></div><div class="static">
A. You can track your packet, by entering your MPD tracking number into the form found at: www.bookmypacket.com</div><div class="static">
<b>Q. Why doesn’t my tracking number work?</b></div><div class="static">
A. All bookmypacket.com™ tracking numbers begin with BMP - you should check that this is the number you are using to request the status of your packet. If you do not have an MPD tracking number, it may be possible that your packet was sent through a courier directly, some of which can be contacted via their websites:</div><div class="static">
<b>Q. My packet is not tracking and states that the order has not been placed, why?</b></div><div class="static">
A. Packets will not be traceable until they have been collected. If the item has been collected and there is still no tracking visible, you should contact our customer support team who will be able to assist.</div><div class="static">
<b>Q. Why will the courier not recognise my tracking number?</b></div><div class="static">
A. Our tracking numbers are bookmypacket.com™ specific, generated by our system when you place an order and as such not recognised by individual courier companies. We can supply courier tracking numbers on request; however, any courier-related query can easily be raised and resolved by contacting our customer support team.</div><div class="static">
<b>Q. My packet is being shown as loaded onto van when it has not yet been collected, why?</b></div><div class="static">
A. This scan refers to the collection paperwork being added to the driver’s route for the day. This is to show that your collection has been assigned to a driver for that day.</div><div class="static">
<b>Q. I see no recent tracking updates, why?</b></div><div class="static">
A. Packets should be scanned every 24 hours to allow live updates on the status of your delivery. Should you receive no tracking updates for over 24 hours, please contact our customer support team at the earliest convenience with the following information to hand:</div><div class="static">
Order Number</div><div class="static">
Packaging used</div><div class="static">
Packet contents (including the make, model, colour and any distinguishing features of your items)</div><div class="static">

Delivery Query</div><div class="static">
<b>Q. What if I am not in when my packet is being delivered?</b></div><div class="static">
A. If you are not at home when your packet is being delivered it is possible for the driver to leave your packet in a safe location or with a neighbour. If this cannot be done the driver will return the packet back to the depot. From this point you should contact our customer support team to re-arrange a delivery for an alternative date.</div><div class="static">
<b>Q. I have received a missed delivery card, how do I get my packet?</b></div><div class="static">
A. You should contact our customer support team who will assist to arrange the delivery on an alternative date to your liking.</div><div class="static">
Please note that re-deliveries require 24 hours notice to be arranged.</div><div class="static">
<b>Q. Can I arrange for my packet to be re-delivered to an alternative address?</b></div><div class="static">
A. You may contact our customer support team to request a new delivery address, but in some cases this is not always possible.</div><div class="static">
Please note that additional surcharges may be applied to your order should you change the delivery address (payment of which, will be required before the delivery request is completed).</div><div class="static">
<b>Q. My packet has not arrived on the expected delivery date, what should I do?</b></div><div class="static">
A. In the first instance you should track your packet on our tracking page to identify whether your packet has unexpectedly been delayed during transit. Although the couriers endeavour to deliver within the correct timescale, it is possible that on some occasion’s unforeseen circumstances can push the delivery date back. Please also be aware that not all services are guaranteed, meaning in some cases we are unable to compensate or refund for late deliveries.</div><div class="static">
For more information, it is always possible to contact our customer support team to assist with your query.</div><div class="static">
<b>Q. The courier is struggling to locate my address for delivery, what can I do to help?</b></div><div class="static">
A. You should contact our customer support team at the earliest possible convenience with your correct and full delivery address to hand, along with any directions that may assist in helping the driver to locate you.</div><div class="static">
<b>Q. What time can I expect my packet to be delivered?</b></div><div class="static">
A. All deliveries are generally scheduled for between 9am and 5.30pm; however, in some cases, this may extend to 7pm.</div><div class="static">
Deliveries may be earlier if a time-specific service has been booked.</div><div class="static">

Returned Status</div><div class="static">
<b>Q. My packet(s) has been returned, why?</b></div><div class="static">
A. Packets may be returned to sender for various reasons, and in most cases the courier will label the box as to why. Unfortunately, when a packet is scheduled for return we cannot stop the process, and a new order will have to be placed for the packet to be re-delivered.</div><div class="static">
In some instances, you may be liable to pay a surcharge for the return journey of your packet – you will be notified if these charges are applicable to you.</div><div class="static">
<b>Q. My packet has been returned, why was I not informed of this?</b></div><div class="static">
A. bookmypacket.com™ are not made aware of every packet that is returned to sender and therefore are not able to advise our customers every time. It is the responsibility of the customer to track their own packet with the tracking number we provide after all orders.</div><div class="static">
<b>Q. I have sent a packet and need to have it returned, what should I do?</b></div><div class="static">
A. You should contact our customer support team, who will arrange the return of your packet. Additional surcharges will be applied to your order for this service, and unfortunately no refund can be issued.</div><div class="static">
<b>Q. My packet is showing as returned but I do not have it, why?</b></div><div class="static">
A. You should contact our customer support team, who will arrange the return of your packet. Additional surcharges will be applied to your order for this service, and unfortunately no refund can be issued.</div><div class="static">
Claims settlement</div><div class="static">
<b>Q. How do I submit a claim for a packet that has arrived damaged, or is missing?</b></div><div class="static"></div><div class="static">
A. A claim form and information on how to submit a claim can be found here. Before submitting a claim please check that your item is not listed on the prohibited/restricted items and that the deadlines for a submitting a claim have not passed.</div><div class="static">
In most cases, our customer support team will be able to assist you with your issue should one arise.</div><div class="static">
<b>Q. How can I submit my claim form to you?</b></div><div class="static">
A. All claims can be submitted online from our claims page.</div><div class="static">
<b>Q. I have submitted my claim but I have not had a response, why?</b></div><div class="static">
A. If you have submitted a claim via our online form you should receive an acknowledgement via e-mail within 24 hours. </div><div class="static">
Unfortunately, bookmypacket.com is subject to strict deadlines for claims to be submitted to the courier and we are not able to accept any claims received outside of these deadlines.</div><div class="static">
<b>Q. I have submitted my claim, what happens now?</b></div><div class="static">
A. A claims assessor will review your claim and a request for any additional information/documents needed shall be sent to you via e-mail. If this request is made you will have 7 days to reply to the e-mail with the necessary information otherwise your claim will be rejected. Upon receipt of all documents, the request will be put forward to the courier.</div><div class="static">
The courier will investigate the claim and respond with a resolution within about 4 weeks after the request is submitted.</div><div class="static">
When a resolution is reached, bookmypacket.com™ will notify you of the outcome via email.
Some claims may take up to and over 4 weeks to be resolved by the courier, but bookmypacket.com will do everything possible to speed up the process.</div><div class="static">
<b>Q. I do not agree with the decision on my claim, what can I do?</b></div><div class="static">
A. The decision on your claim will have been made by the courier that has shipped your packet. If you do not agree with the decision made we would invite you to make an appeal on the decision via claims@bookmypacket.com, including any additional information and documents to support your appeal.</div><div class="static">
We cannot guarantee that an appeal will alter the decision made by our couriers, but we will endeavour to fight the cause on your behalf!</div><div class="static">
<b>Q. What will I be able to claim for?</b></div><div class="static">
A. You will be able to claim for the cost of the item or repair of the item sent, up to the value of the compensation cover on your order. All services come with standard compensation, which you may top-up during the booking process to match the value of the item you are sending.</div><div class="static">
Please note, that if items are not covered for the full amount we will be unable to for the full value of the item from the courier. For more information on compensation cover please see our compensation cover guide.</div><div class="static">

Refunds status</div><div class="static">
<b>Q. Can I claim a refund for my order that was not collected?</b></div><div class="static">
A. In cases where the courier has attempted to collect your packet and charged for the service, we will not be able to refund your payment. In all other instances when you have requested a refund at least 24 hours in advance of collection, a refund may be processed.</div><div class="static">
<b>Q. The driver refused to take my item, can I claim a refund?</b></div><div class="static">
A. Unfortunately, we are not able to process refunds for items that have been refused collection. Collections can be refused by the driver for a number of reasons, including insufficient/incorrect packaging, no label attached, item too large, item too heavy etc.</div><div class="static">
If the driver incorrectly refused to collect your item, you should contact our customer support team who will happy to assist in resolving the issue for you.</div><div class="static">
<b>Q. How long do refunds take to process?</b></div><div class="static">
A. Refunds are usually processed within 3-5 working days after the original scheduled collection date.</div><div class="static">
<b>Q. My refund has been refused, how can I appeal?</b></div><div class="static">
A. If you believe your refund request has been unfairly denied, feel free to e-mail us at customersupport@bookmypacket.com along with any supporting documentation or information, and we will be happy to resolve the issue with you.</div><div class="static">
We aim to respond to these within 5 working days when all investigations have been completed.</div><div class="static">

Customer Support</div><div class="static">
<b>Q. How can I contact the customer support team?</b></div><div class="static">
A. You can contact customer support via the contact us page here</div><div class="static">
<b>Q. What are the customer support working hours?</b></div><div class="static">
A. Our customer support team is available Monday to Friday 9.30 am – 6.30 pm.</div><div class="static">
<b>Q. How do I make a complaint?</b></div><div class="static">
A. We are sorry to hear when a customer is not happy, and would like every opportunity to put things right. Should you wish to complain against any aspect of our service, you can do so by following the following procedure:</div><div class="static">
Report your complaint to our customer support team either via e-mail to customersupport@bookmypacket.com, letter to Complaints Team, bookmypacket.com Ltd, </div><div class="static">
If raising the complaint via phone or live help, the advisor will take the full details of the complaint, which will be passed on to the customer service manager for investigation.</div><div class="static">
Any complaint that is raised with bookmypacket.com™ will be taken very seriously, and a full investigation into the matter will be conducted.</div><div class="static">
A full response to your complaint should be received within 7 working days, giving us sufficient time to fully investigate every aspect of the complaint made and respond appropriately.</div><div class="static">
Response to all complaints will usually be via e-mail or letter.</div>

