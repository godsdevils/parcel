<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
<title>India's first online courier service for domestics and International</title>
<link rel="Stylesheet" href="<?php echo $base_url; ?>css/main.css">
<link rel="Stylesheet" href="<?php echo $base_url; ?>css/innerPages.css">
<link rel="Stylesheet" href="<?php echo $base_url; ?>css/styles.css">
<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>js/html5.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>js/script.js"></script>
</head>
<body>

<!----------------------------------- Start Main Section ----------------------------------->

<div id="mainCont">

    <!----------------------------------- Start Header Section ----------------------------------->
  <header id="HdrCont">
    <a href="/"><h1 class="logo"> Packet</h1></a> 
    <?php if(empty($_SESSION['uid'])){  ?>
    <ul class="toplink">
      <li><a href="/login">Register</a></li>
      <li><a href="/login">Sign In</a></li>
    </ul>
    <?php } else {  ?>
    <ul class="toplink">
      <li><a href="/logout">logout</a></li>
      <li><a href="">Welcome <?php echo $_SESSION['display_name']; ?></a></li>
      </ul>
    <?php } ?>
    <div id="Search">
      <h2>Packet Tracking</h2>
      <form action="/parceltracking" method="post">
      <input type="text" name="txtTracking" class="search" id="txtTracking" class="trackItInput" />
      <input type="image" alt="Submit" value="btnSubmitReg" name="btnSubmitReg" id="btnSubmitReg" src="<?php echo $base_url; ?>images/go.gif" />
      </form>
    </div>
    <br class="clr" />
  </header>
  
    <!----------------------------------- End Header Section ----------------------------------->
    <!----------------------------------- Start Navigation Section ----------------------------------->

  <nav>
    <ul class="nav">
      <li><a href="<?php echo $base_url ?>info">Home</a></li>
      <li><a href="<?php echo $base_url ?>staticpages/courierservice">Courier Services</a></li>
      <li><a href="<?php echo $base_url ?>staticpages/couriercompanies">Courier Companies</a></li>
      <li><a href="<?php echo $base_url ?>staticpages/tracking">Tracking</a></li>
      <li><a href="<?php echo $base_url ?>staticpages/bulkshipping">BUlk Shipping</a></li>
      <li class="last"><a href="<?php echo $base_url ?>staticpages/help">Help &amp; Contact</a></li>
      <br class="clr" />
    </ul>
  </nav>

    <!----------------------------------- End Navigation Section ----------------------------------->

