<?php
class Calc extends CI_Model
{
  private $objDB;
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('calcdblayer'));
    //$this->load->library(array('CurlHandling'));
    //$this->load->helper(array('common'));
    $this->objDB = $this->load->database('default', TRUE);
    //$this->objDBCMS = $this->load->database('cms', TRUE);
    $this->config->load('config');
    $this->conf = $this->config->config;
    //$this->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'memcached'));
  }
  function processCourierList($data)
  {
    $sPincode = $data['source-pincode'];
    $dPincode = $data['destination-pincode'];
    if((float)$data['length']*(float)$data['width']*(float)$data['height']/5000 > (float)$data['weight'])
      $cWeight = (float)$data['length']*(float)$data['width']*(float)$data['height'];
    else
      $cWeight = (float)$data['weight'];
    $aWeight = (float)$data['nparcel']*$cWeight;
    $selectedCourier = '';
    $arr = $this->calcdblayer->getIdCourier($sPincode,$dPincode);
    foreach($arr as $dkey=>$data)
    {
      $selectedCourier .= $data['courier_id'].',';
    }
    $arr = $this->calcdblayer->getCourierNameImage($arr); 
    $arr = $this->getCourierCharges($arr,$aWeight);
    $arr['parcelWeight'] = $aWeight;
    return $arr;
  }
  function getCourierCharges($arr,$wt)
  {
    foreach($arr as $key=>$val)
    { 
      if($val['source_city'] == $val['dest_city'])
        $arr[$key]['charges'] = $this->calcdblayer->getCharges($val,$geo='intracity_local',$wt);
      elseif ($val['source_zone_id'] == $val['dest_zone_id'])
        $arr[$key]['charges'] = $this->calcdblayer->getCharges($val,$geo='intracity',$wt);
      elseif($val['dest_zone_id'] == 5)
        $arr[$key]['charges'] = $this->calcdblayer->getCharges($val,$geo='north_east',$wt);
      else
        $arr[$key]['charges'] = $this->calcdblayer->getCharges($val,$geo='roi',$wt);
      $arr[$key]['net_charges'] = round($this->calcdblayer->getNetCharges($arr[$key]['courier_id'],$arr[$key]['charges']));
    }
    return $arr;
  }

}
