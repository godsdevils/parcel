<?php
class Userlogin extends CI_Model
{
  private $objDB;
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('logindblayer'));
    $this->config->load('config');
    $this->conf = $this->config->config;
  }
  function IdentifyUser($data)
  {  
    if(!empty($data))
    {
      $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
      if (preg_match($pattern, $data['email']) === 1) 
      {
        $email = $data['email'];
        $mobile = 0;
        $name = '';
      }
      elseif(preg_match('/^([0-9]{10})$/',$data['email']))
      {
        $mobile = $data['email'];
        $email = '';
        $name = '';
      }
      else
      {
        $mobile = 0;
        $email = '';
        $name = $data['email'];
      }
      $userInfo = $this->logindblayer->bIdentifyUser($email,$mobile,$name);
      if(!empty($userInfo))
      { 
        if($userInfo['password'] == $data['password'])
        {
          //$aUserProfileInfo = $this->logindblayer->aGetUserProfileInfo($userInfo['id']);
          $_SESSION['display_name'] = $userInfo['display_name'];
          $_SESSION['uid'] = $userInfo['id'];
          $_SESSION['email'] = $userInfo['email'];
          header("Location: ".$this->conf['base_url']."info", true, 301);
          exit();
        }
        else
        {
          $_SESSION['error'] = 'Please Enter Correct Password.';
          return;
        }
      }
      else
      {
        $_SESSION['error'] = 'You are not registered with us.';
        return;
      }
    }
    else
    {
      $_SESSION['error'] = 'Something Caused this Error. Please try Again!';
      return;
    }
  }
}
