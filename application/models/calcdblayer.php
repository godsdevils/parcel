<?php
class Calcdblayer extends CI_Model
{
  private $objDB;
  function __construct()
  {
    parent::__construct();
    //$this->load->model(array('searchbo','sellerbo','tagbo','categorybo','sellerbo','seobo','promobo','affiliatebo','sitemapbo'));
    //$this->load->library(array('CurlHandling'));
    //$this->load->helper(array('common'));
    $this->objDB = $this->load->database('default', TRUE);
    //$this->objDBCMS = $this->load->database('cms', TRUE);
    $this->config->load('config');
    $this->conf = $this->config->config;
    //$this->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'memcached'));
  }
  function getIdCourier($sPincode,$dPincode)
  {
    $base_url = $this->conf['base_url'];
    $sSql = "select * from courier_service where pin = '".$sPincode."' and source = 't'";
    $oQueryResult = $this->objDB->query($sSql);
    $sResult = $oQueryResult->result_array();
    $selectedCourier = '';
    $arr = array();
    if(count($sResult) > 0)
    {
      foreach($sResult as $key=>$val)
      {
        $arr[$val['name_id']]['courier_id'] = $val['name_id'];
        $arr[$val['name_id']]['source_pincode'] = $sPincode;
        $arr[$val['name_id']]['source_zone_id'] = $val['zone_id'];
        $arr[$val['name_id']]['source_code'] = $val['code'];
        $arr[$val['name_id']]['source_city'] = $val['city'];
        $selectedCourier .= $val['name_id'].',';
      }
    }
    else
    {
      $_SESSION['error'] = 'Service Not availabe at '.$sPincode;
      header("Location: ".$base_url."info");
      exit();
    }
    $dSql = "select * from courier_service where pin = '".$dPincode."' and destination = 't' and name_id In (".substr($selectedCourier,0,-1).")";
    $oQueryResult = $this->objDB->query($dSql);
    $dResult = $oQueryResult->result_array();
    if(count($dResult) > 0)
    {
      foreach($dResult as $dkey=>$value)
      {
        $arr[$value['name_id']]['dest_pincode'] = $dPincode;
        $arr[$value['name_id']]['dest_zone_id'] = $value['zone_id'];
        $arr[$value['name_id']]['dest_code'] = $value['code'];
        $arr[$value['name_id']]['dest_city'] = $value['city'];
      }
    }
    else
    {
      $_SESSION['error'] = 'Service Not availabe at '.$dPincode;
      header("Location: ".$base_url."info");
      exit();
    }
    return $arr;
  }
  function getCourierNameImage($arr)
  {
    $selectedCourier = '';
    foreach($arr as $data)
      $selectedCourier .= $data['courier_id'].',';
    $nSql = 'select * from res_partner where id In ('.substr($selectedCourier,0,-1).')';
    $oQueryResult = $this->objDB->query($nSql);
    $nResult = $oQueryResult->result_array();
    foreach($nResult as $key=>$val)
    {
      $arr[$val['id']]['name'] = $val['name'];
      $arr[$val['id']]['image'] = $val['image'];
    }
    return $arr;
  }
  function getCharges($arr,$geo,$wt)
  {
    $wt = 1000*$wt;
    $nSql = "select ".$geo." from basic_rate where courier_company = ".$arr['courier_id'];
    $oQueryResult = $this->objDB->query($nSql);
    $nResult = $oQueryResult->result_array();
    if($nResult[0][$geo] > 0)
    {
      $nResult[0][$geo]  = ceil($wt/500)*$nResult[0][$geo];
      return $nResult[0][$geo];
    }
    else
      $_SESSION['error'] = 'Charges Not available.';
    return 0;
  }
  function getNetCharges($iD,$charges)
  {
    $nSql = "select tax , fuel_sc from weight_zone_rate where courier_company = ".$iD;
    $oQueryResult = $this->objDB->query($nSql);
    $nResult = $oQueryResult->result_array();

    return ($charges + $charges*$nResult[0]['fuel_sc']/100) + $nResult[0]['tax']*($charges + $charges*$nResult[0]['fuel_sc']/100)/100;

  }
}
