<?php
class Getinfo extends ci_model
{
  public $objdb;
  function __construct()
  {
    parent::__construct();
    $this->objdb = $this->load->database('default', true);
    $this->config->load('config');
    $this->conf = $this->config->config;
  }
  function parcelTrack($awb)
  {
    $sSql = "select * from courier_statous_list where bmp_awb_number = '".$awb."' order by date desc";
    $oQueryResult = $this->objDB->query($sSql);
    $sResult = $oQueryResult->result_array();
    return $sResult;
  }
  function parcelHistoryTrack($awb)
  {
    $sSql = "select * from courier_statous_list where bmp_awb_number IN (".$awb.") order by date desc";
    $oQueryResult = $this->objDB->query($sSql);
    $sResult = $oQueryResult->result_array();
    return $sResult;
  }
  function processHistory($sResult)
  {
    $temp = array();
    foreach($sResult as $key=>$val)
    {
      if(!array_key_exists($val['order_id'],$temp))
        $temp[$val['order_id']] = $val;
    }
    return $temp;
  }
}
