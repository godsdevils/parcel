<?php

include_once("xmlrpc.inc");

class OpenERPXmlrpc {

   private $user, $password, $database, $services, $client, $res, $msg, $id;

   function __construct($usr, $pass, $db, $server) {

      $this->user = $usr;
      $this->password = $pass;
      $this->database = $db;
      $this->services = $server;

      $this->client = new xmlrpc_client($this->services.'common');

      $this->msg = new xmlrpcmsg('login');
      $this->msg->addParam(new xmlrpcval($this->database, "string"));
      $this->msg->addParam(new xmlrpcval($this->user, "string"));   
      $this->msg->addParam(new xmlrpcval($this->password, "string"));

      $this->res =  &$this->client->send($this->msg,20);

      if(!$this->res->faultCode()){

         $this->id = $this->res->value()->scalarval();

      }
      else {

         echo "Unable to login ".$this->res->faultString();
         exit;
      }
   }

   function read($post = null) {

      $this->client = new xmlrpc_client($this->services.'object');
      if(empty($post)) {

         $ids_read = array(new xmlrpcval('1', 'int'), new xmlrpcval('2', 'int'));

         $key = array(new xmlrpcval('id','integer') , new xmlrpcval('name', 'string'));

         $this->msg = new xmlrpcmsg('execute');
         $this->msg->addParam(new xmlrpcval($this->database, "string"));
         $this->msg->addParam(new xmlrpcval(1, "int"));
         $this->msg->addParam(new xmlrpcval($this->password, "string"));
         $this->msg->addParam(new xmlrpcval("res.partner","string"));
         $this->msg->addParam(new xmlrpcval("read", "string"));
         $this->msg->addParam(new xmlrpcval($ids_read, "array"));
         $this->msg->addParam(new xmlrpcval($key, "array"));

         $this->res = &$this->client->send($this->msg);

         if(!$this->res->faultCode()) {

            $read_html = '<table width="20%" border="0" cellpadding="4" cellspacing="4" align="center">
                        <tr>
                           <th>Id</th>
                           <th>Name</th>
                        </tr>
                     ';

            $scalval = $this->res->value()->scalarval();

            foreach ($scalval as $keys => $values) {

               $value = $values->scalarval();

               $read_html .= '
                     <tr>
                        <td>'.$value['id']->scalarval().'</td>
                        <td>'.$value['name']->scalarval().'</td>
                     </tr>
                     <tr><td colspan="2"></td></tr>
                        ';
            }

            $read_html .= '
                     </table>
                     ';

            return $read_html;
         }
         else {
            return "Not read recode from partner table <br />".$this->res->faultString();
         }
      }
   }
   function createRecord()
   {
      $this->client = new xmlrpc_client($this->services.'object');
      if(empty($post)) 
      {
         $vals = array(
         'src_pin'=>new xmlrpcval('110001', "string") ,
         'src_city_code'=>new xmlrpcval('DEL', "string") ,
         'dis_pin'=>new xmlrpcval('122001', "string") ,
         'dis_city_code'=>new xmlrpcval('GGN', "string") ,
         'weight'=>new xmlrpcval('20' , "string")
         );
         $this->msg = new xmlrpcmsg('execute');
         $this->msg->addParam(new xmlrpcval($this->database, "string"));
         $this->msg->addParam(new xmlrpcval($this->id, "int"));
         $this->msg->addParam(new xmlrpcval($this->password, "string"));
         $this->msg->addParam(new xmlrpcval("api.object","string"));
         $this->msg->addParam(new xmlrpcval("minimum_price", "string"));
         $this->msg->addParam(new xmlrpcval($vals, "struct"));

         $this->res = &$this->client->send($this->msg);
         if(!$this->res->faultCode()) 
         {
            $scalval = $this->res->value()->scalarval();
            foreach ($scalval as $keys => $values) 
               $value = $values->scalarval();
            return $value;
         }
         else 
            return "Not read recode from partner table <br />".$this->res->faultString();
      }
   }
}

//$cnt = new OpenERPXmlrpc('admin', 'admin', 'Courier', 'http://10.10.1.4:8069/xmlrpc/');
$cnt = new OpenERPXmlrpc('admin', 'admin', 'Courier', 'http://http://182.18.176.126:8069/xmlrpc/');

echo $cnt->read();
//echo $cnt->createRecord();
?>
