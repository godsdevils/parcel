<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logout extends CI_Controller {
    /**
     * Set base url And call model
     */
    function __construct() {
        parent::__construct();
        session_start();
        $this->config->load('config');
        $this->conf = $this->config->config;
    }
    
    /**
     *  Login of user
     */
  function index()
  {
    $data = array();
    $data['base_url'] = $this->conf['base_url'];
    unset($_SESSION);
    session_unset();
    session_destroy();
    session_regenerate_id(true);
    header("Location: ".$data['base_url']."info");
    exit();
  }
}
