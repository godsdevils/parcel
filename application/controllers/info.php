<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Info extends CI_Controller {
    /**
     * Set base url And call model
     */
    public $objDB;
    function __construct() {
        parent::__construct();
        session_start();
        $this->load->helpers('form_helper');
        $this->objDB = $this->load->database('default', TRUE);
        $this->config->load('config');
        $this->conf = $this->config->config;
        $this->load->model('getinfo');
    }
    
    /**
     *  Login of user
     */
    function index()
    {
      $data = array();
      $data['base_url'] = $this->conf['base_url'];
      $this->load->view('header',$data); 
      $this->load->view('msg',$data); 
      $this->load->view('info',$data); 
      $this->load->view('footer',$data); 
    }
}
