<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {
    /**
     * Set base url And call model
     */
    public $objDB;
    function __construct() {
        parent::__construct();
        session_start();
        $this->load->helpers('form_helper');
        $this->objDB = $this->load->database('default', TRUE);
        $this->config->load('config');
        $this->conf = $this->config->config;
        $this->load->model(array('getinfo','userlogin'));
    }
    
    /**
     *  Login of user
     */
    function index()
    {
      $data = array();
      $data['base_url'] = $this->conf['base_url'];
      if(isset($_SESSION['uid']))
      {
        header("Location: ".$this->conf['base_url']."info");
        exit();
      }
      if($_POST)
      {
        $str = $this->userlogin->IdentifyUser($_POST);
        $data['warning'] = $str;
      }
      //print_r($this->getinfo->getappointmentdetails());die;
      $this->load->view('header',$data); 
      $this->load->view('msg',$data); 
      $this->load->view('login',$data); 
      $this->load->view('footer',$data); 
    }
}
