<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calculation extends CI_Controller {
    /**
     * Set base url And call model
     */
    function __construct() {
        parent::__construct();
        session_start();
        $this->load->helpers('form_helper');
        $this->load->model(array('calc'));
        $this->config->load('config');
        $this->conf = $this->config->config;
//        $this->load->model('User_model');
    }
    
    /**
     *  Login of user
     */
    function index()
    {
      $data = array();
      $data['base_url'] = $this->conf['base_url'];
      if($_POST)
      { 
        if(!preg_match('/^\d{6}$/',$_POST['source-pincode']) || !preg_match('/^\d{6}$/',$_POST['destination-pincode']))
        {
          $_SESSION['error'] = 'Please Enter Correct Format of Pincode.';
          header("Location: ".$data['base_user']."info", true, 301);
          exit();
        }

        foreach($_POST as $key=>$val)
        {
          if(empty($val) && $key != 'sourcecode' && $key != 'destinationcode')
          {
            $_SESSION['error'] = 'Please fill '.$key.' data.';
            header("Location: ".$data['base_user']."info", true, 301);
            exit();
          }
        }
        if(empty($data['ERROR']))
        {
          $data['courierList'] = $this->calc->processCourierList($_POST);
          $data['parcelWeight'] = $data['courierList']['parcelWeight'];
          unset($data['courierList']['parcelWeight']);
        }
        else
        {
          $_SESSION['error'] = $data['ERROR'];
          header("Location: ".$data['base_url'].'info');
          exit();
        }
      $this->load->view('header',$data); 
      $this->load->view('msg',$data); 
      $this->load->view('calculation',$data); 
      $this->load->view('footer',$data); 
      }
      else
      {
        $this->load->view('header',$data); 
        $this->load->view('error');
        $this->load->view('footer',$data); 
      }
    }
}
