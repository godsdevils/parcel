<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parceltracking extends CI_Controller {
    /**
     * Set base url And call model
     */
    public $objDB;
    function __construct() {
        parent::__construct();
        session_start();
        $this->load->helpers('form_helper');
        $this->objDB = $this->load->database('default', TRUE);
        $this->config->load('config');
        $this->conf = $this->config->config;
        $this->load->model('getinfo');
    }
    
    /**
     *  Login of user
     */
    function index()
    {
      $data = array();
      $data['base_url'] = $this->conf['base_url'];
      if($_POST)
      {
	$_POST['txtTracking'] = str_replace(' ', '', $_POST['txtTracking']);
        if(preg_match('/^[0-9,]+$/',$_POST['txtTracking']) > 0)
        {
          if (strpos($_POST['txtTracking'],',') > 0)
          {
            $tempArr = array();
            $tempArr = explode(',',$_POST['txtTracking']);
            $str = "'";
            foreach($tempArr as $val)
            {
              if(!empty($val))
                $str .= $val."','"; 
            }
            $str = substr($str,0,-2);
            $data['multiple'] = 1;
            $result = $this->getinfo->parcelHistoryTrack($str);
            if(!empty($result))
              $data['track'] = $this->getinfo->processHistory($result);
            else
              $_SESSION['error'] = 'AWB Number Not Found';

          }
          else
          {
            $data['multiple'] = 0;
            $data['track'] = $this->getinfo->parcelTrack($_POST['txtTracking']);
          }
          $this->load->view('header',$data); 
          $this->load->view('msg',$data); 
          $this->load->view('parceltracking',$data); 
          $this->load->view('footer',$data); 
        }
        else
        {
          $_SESSION['error'] = 'Please Enter Valid AWB Number.';
          $this->load->view('header',$data); 
          $this->load->view('msg',$data); 
          $this->load->view('footer',$data); 
        }
      }
      else
      {
        $this->load->view('header',$data); 
        $this->load->view('error');
        $this->load->view('footer',$data); 
      }
    }
}
