<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class StaticPages extends CI_Controller {
    /**
     * Set base url And call model
     */
    public $objDB;
    function __construct() {
        parent::__construct();
        session_start();
        $this->load->helpers('form_helper');
        $this->objDB = $this->load->database('default', TRUE);
        $this->config->load('config');
        $this->conf = $this->config->config;
        $this->load->model('getinfo');
    }
    
    /**
     *  Login of user
     */
    function index()
    {
      $data = array();
      $data['base_url'] = $this->conf['base_url'];
      $this->load->view('header',$data); 
      $this->load->view('footer',$data); 
    }
    function aboutus()
    {            
      $data = array();
      $data['base_url'] = $this->conf['base_url'];

      $this->load->view('header',$data); 
      $this->load->view('aboutus',$data); 
      $this->load->view('footer',$data); 
    }
    function bulkshipping()
    {            
      $data = array();
      $data['base_url'] = $this->conf['base_url'];

      $this->load->view('header',$data); 
      $this->load->view('bulkshipping',$data); 
      $this->load->view('footer',$data); 
    }
    function couriercompanies()
    {            
      $data = array();
      $data['base_url'] = $this->conf['base_url'];

      $this->load->view('header',$data); 
      $this->load->view('couriercompanies',$data); 
      $this->load->view('footer',$data); 
    }
    function courierservice()
    {            
      $data = array();
      $data['base_url'] = $this->conf['base_url'];

      $this->load->view('header',$data); 
      $this->load->view('courierservice',$data); 
      $this->load->view('footer',$data); 
    }
    function faq()
    {            
      $data = array();
      $data['base_url'] = $this->conf['base_url'];

      $this->load->view('header',$data); 
      $this->load->view('faq',$data); 
      $this->load->view('footer',$data); 
    }
    function help()
    {            
      $data = array();
      $data['base_url'] = $this->conf['base_url'];

      $this->load->view('header',$data); 
      $this->load->view('help',$data); 
      $this->load->view('footer',$data); 
    }
    function tracking()
    {            
      $data = array();
      $data['base_url'] = $this->conf['base_url'];

      $this->load->view('header',$data); 
      $this->load->view('tracking',$data); 
      $this->load->view('footer',$data); 
    }
}
