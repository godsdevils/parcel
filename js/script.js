$(document).ready(function() {

    $(function() {
        $(".tab-content").hide();
        var activetab = $(".tab-active").attr('id');
        $('#' + activetab + '-content').show();
        $(".tab").click(function() {
            var activetab = $(".tab-active").attr('id');
            $('#' + activetab).removeClass('tab-active');
            $(".tab-content").hide();
            var myid = $(this).attr('id');
            $('#' + myid).addClass('tab-active');
            $('#' + myid + '-content').show();
        });

        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 5000,
            values: [500, 3000],
            slide: function(event, ui) {
                $("#amount").val("Rs " + ui.values[ 0 ] + " - Rs " + ui.values[ 1 ]);
            }
        });
        $("#amount").val("Rs " + $("#slider-range").slider("values", 0) + " - Rs " + $("#slider-range").slider("values", 1));

        $('#add-clinic').click(function() {
            $('#add-clinic-form').clone().appendTo('#clinics-details').removeAttr('class').removeAttr('style');
        });
        $('#add-receptionist-1').click(function() {
            $('#add-receptionist-form').clone().appendTo('#receptionist-details-1').removeAttr('class').removeAttr('style');
        });
        $('#add-receptionist-2').click(function() {
            $('#add-receptionist-form').clone().appendTo('#receptionist-details-2').removeAttr('class').removeAttr('style');
        });
        $('#add-clinic-1-shift').click(function() {
            $('#clinic-1-shift').clone().appendTo('#clinic-1').removeAttr('class').removeAttr('style');
        });
        $('#add-clinic-2-shift').click(function() {
            $('#clinic-2-shift').clone().appendTo('#clinic-2').removeAttr('class').removeAttr('style');
        });
        $('#add-another-doctor-link').click(function() {
            $('#rs-doctor-add').after($('#rs-doctor-add-blank').clone().removeClass('hidden').removeAttr('style'));
        });
        var dates1 = [10, 12, 30, 24, 15, 26];
        $("#datepicker").datepicker({
            dateFormat: 'd/m/yy|d MM, yy',
            beforeShowDay: function(date) {
                var theday = date.getDate();
                if ($.inArray(theday, dates1) == -1)
                    return [true, ""];
                return [true, "highlighted"];
            },
            onSelect: function(dateText, inst) {
                $('#todays_schedule').trigger('click');
                var i = dateText.indexOf('|');
                var selectedDate = dateText.slice(i + 1);
                $("#d-ts-date").html(selectedDate);
            }
        });
//        $(function() {
//            var day1 = $("#datepicker").datepicker().val();
//            var i = day1.indexOf('|');
//            var date1 = day1.slice(i + 1);
//            $("#d-ts-date").html(date1);
//        });
        $('#edit-personal-details').click(function() {
            $('#personaldetail1').hide();
            $('#personaldetail1edit').show();
        });
        $('#edit-clinic-details').click(function() {
            $('#clinicdetail1').hide();
            $('#clinicdetail1edit').show();
            $('#clinicdetail2').hide();
            $('#clinicdetail2edit').show();
        });
        $('#edit-receptionist-1-clinic-1').click(function() {
            $('#receptionist-1-clinic-1').hide();
            $('#receptionist-1-clinic-1-edit').show();
        });
        $('#edit-receptionist-2-clinic-1').click(function() {
            $('#receptionist-2-clinic-1').hide();
            $('#receptionist-2-clinic-1-edit').show();
        });
        $('#edit-receptionist-2-clinic-2').click(function() {
            $('#receptionist-2-clinic-2').hide();
            $('#receptionist-2-clinic-2-edit').show();
        });
        $('#add-symptoms').click(function() {
            $('#symptoms-1').clone().appendTo('#symptoms');
        });
        $('#add-prognosis').click(function() {
            $('#prognosis-1').clone().appendTo('#prognosis');
        });
        $('#add-more-medicine-link').click(function() {
            $('#add-more-medicine-list-1').clone().appendTo('#add-more-medicine-list');
        });
        var dates = [1, 12, 3, 14, 5, 26];
        $("#calender").datepicker({
            dateFormat: 'd/m/yy|d MM, yy',
            beforeShowDay: function(date) {
                var theday = date.getDate();
                if ($.inArray(theday, dates) == -1)
                    return [true, ""];
                return [true, "highlighted"];
            },
            onSelect: function(dateText) {
                var i = dateText.indexOf('|');
                var selectedDate = dateText.slice(i + 1);
                $("#d-ts-date").html(selectedDate);
            }
        });
        $(function() {
            var day1 = $("#calender").datepicker({dateFormat: 'd MM, yy'}).val();
            try{
            var i = day1.indexOf('|');
            var date1 = day1.slice(i + 1);
            }catch(e){}
            $("#d-ts-date").html(date1);
        });
        
        $('#calender_dob').datepicker({
            dateFormat: 'd, M, yy',
            changeYear: true,
            yearRange: '1961:' + new Date().getFullYear(),
            onSelect: function(dateText) {
                var dobDay = $("#calender_dob").datepicker('getDate').getDate();
                $('.dob-cal-day').val(dobDay);
                var dobMonth = $("#calender_dob").datepicker('getDate').getMonth();
                $('.dob-cal-month').val(dobMonth);
                dobYear = $("#calender_dob").datepicker('getDate').getFullYear();
                $('.dob-cal-year').val(dobYear);
                $(this).val('').blur();
            }
        });
        $('#_calender').datepicker({changeYear: true, yearRange: '1961:' + new Date().getFullYear()});


        $("#engage").click(function() {
            $("#engage").addClass('hidden');
            $("#disengage").show();
            $('#not-available').html('&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp');

        });
    });
    $("#edit").click(function() {
        $("#patient_name").removeAttr('disabled');
        $("#year").removeAttr('disabled');
        $("#month").removeAttr('disabled');
        $("#day").removeAttr('disabled');
        $("#patient_tel").removeAttr('disabled');
        $("#patient_email").removeAttr('disabled');
        $("#calender_dob").removeAttr('disabled');
    });
    $("#receptionist-cancel-text").click(function() {
        $("#receptionist-name").removeAttr('disabled');
        $("#cal-year").removeAttr('disabled');
        $("#cal-month").removeAttr('disabled');
        $("#cal-day").removeAttr('disabled');
        $("#receptionist-phone").removeAttr('disabled');
        $("#receptionist-email").removeAttr('disabled');
        $("#calender_dob").removeAttr('disabled');
    });
    $("#doctor-cancel-text").click(function() {
        $("#doctor-name").removeAttr('disabled');
        $("#doctor-email").removeAttr('disabled');
        $("#clinic-name").removeAttr('disabled');
        $("#clinic-address").removeAttr('disabled');
    });

    $("#calender_start").datepicker({
        changeYear: true,
        onSelect: function(dateText) {
            $(this).val('').blur();
            var newDate = new Date(dateText);
            var fullDate = $.datepicker.formatDate("d MM,yy", newDate);
            $('#start_date').text(fullDate);
            var selYear = newDate.getUTCFullYear();
            var selDay = newDate.getUTCDate();
            var selMonth = newDate.getUTCMonth();
            //Date.UTC(year,month,day,hours,minutes,seconds,millisec)
            startDate = Date.UTC(selYear, selMonth, selDay);
            //Redraw chart
            printChart(startDate, endDate);
        }
    });

    $("#calender_end").datepicker({
        changeYear: true,
        onSelect: function(dateText) {
            $(this).val('').blur();
            var newDate = new Date(dateText);
            var fullDate = $.datepicker.formatDate("d MM,yy", newDate);
            $('#end_date').text(fullDate);
            var selYear = newDate.getUTCFullYear();
            var selDay = newDate.getUTCDate();
            var selMonth = newDate.getUTCMonth();
            //Date.UTC(year,month,day,hours,minutes,seconds,millisec)
            endDate = Date.UTC(selYear, selMonth, selDay);
            //Redraw chart
            printChart(startDate, endDate);
        }
    });
});




